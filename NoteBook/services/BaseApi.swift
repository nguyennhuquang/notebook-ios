
//
//  BaseApi.swift
//  LaziApp
//
//  Created by Manh on 10/15/18.
//  Copyright © 2018 Manh. All rights reserved.
//

import UIKit
import Alamofire

class BaseApi: NSObject {
    static var baseUrl = "http://notebook.mesaenglish.com/"
    
    var header : HTTPHeaders = [:]
    var params  : Parameters = [:]
    var method : String = ""
    var source : String = ""
    var request : Request? = nil
    init(source : String) {
        self.source = source
    }
    
    func get() -> String {
        return BaseApi.baseUrl + self.source
    }
    
    func setHeader(header: HTTPHeaders){
        self.header = header
    }
    
    func getHeader() -> HTTPHeaders {
        return header
    }
    
    func setParams(params: Parameters){
        self.params = params
    }
    
    func getParams() -> Parameters {
        return params
    }
}


