//
//  CalenderApiService.swift
//  LaziApp
//
//  Created by Manh on 10/15/18.
//  Copyright © 2018 Manh. All rights reserved.
//

import UIKit
import Alamofire

class ApiService: BaseApi {
    
    static let getCategory:String = "api/v1/get-category"
    static let getGrammar:String = "api/v1/get-grammar"
    
    override init(source: String) {
        super.init(source: source)
    }
    
    static func getAllCategory(dataCallback:@escaping (_ data : Any) -> (), errorCallback:@escaping (_ data : Any) -> () ) {
        let calenderService = ApiService.init(source: getCategory)
        URLCache.shared.removeAllCachedResponses()
        Alamofire.request(
            calenderService.get(),
            method: .get
            )
            .responseJSON {
                (response) in
                guard response.result.isSuccess else {
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    errorCallback("Error")
                    return
                }
                dataCallback(response.value as Any)
                
        }
    }
    
    static func getAllGrammar(dataCallback:@escaping (_ data : Any) -> (), errorCallback:@escaping (_ data : Any) -> () ) {
        let calenderService = ApiService.init(source: getGrammar)
        URLCache.shared.removeAllCachedResponses()
        Alamofire.request(
            calenderService.get(),
            method: .get
            )
            .responseJSON {
                (response) in
                guard response.result.isSuccess else {
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    errorCallback("Error")
                    return
                }
                dataCallback(response.value as Any)
                
        }
    }
}
