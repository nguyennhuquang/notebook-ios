//
//  Toast+Intent ViewController.swift
//  EduTech
//
//  Created by Thành Sói on 2019-03-17.
//  Copyright © 2019 Thành Sói. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation

var player: AVPlayer!
var audioPlayer : AVAudioPlayer!

extension UIViewController {
    
    public func showToast(message : String) {
        let toastView = UILabel()
        toastView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        toastView.textColor = UIColor.white
        toastView.textAlignment = .center
        toastView.font = UIFont.preferredFont(forTextStyle: .caption1)
        toastView.layer.cornerRadius = 10
        toastView.layer.masksToBounds = true
        toastView.text = message
        toastView.numberOfLines = 0
        toastView.alpha = 0
        toastView.translatesAutoresizingMaskIntoConstraints = false

        let window = UIApplication.shared.keyWindowInConnectedScenes
        window?.addSubview(toastView)

        let horizontalCenterContraint: NSLayoutConstraint = NSLayoutConstraint(item: toastView, attribute: .centerX, relatedBy: .equal, toItem: window, attribute: .centerX, multiplier: 1, constant: 0)
        let widthContraint: NSLayoutConstraint = NSLayoutConstraint(item: toastView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 250)
        let verticalContraint: [NSLayoutConstraint] = NSLayoutConstraint.constraints(withVisualFormat: "V:|-(>=200)-[loginView(==40)]-20-|", options: [.alignAllCenterX, .alignAllCenterY], metrics: nil, views: ["loginView": toastView])

        NSLayoutConstraint.activate([horizontalCenterContraint, widthContraint])
        NSLayoutConstraint.activate(verticalContraint)

        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            toastView.alpha = 1
        }, completion: nil)

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.8, execute: {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
                toastView.alpha = 0
            }, completion: { finished in
                toastView.removeFromSuperview()
            })
        })
    }

    /// function push Navigation
    public func NavigationView(Identifier: String, animated:Bool ) {
        let sto: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home = sto.instantiateViewController(withIdentifier: Identifier) as UIViewController
        self.navigationController?.pushViewController(home, animated: animated)
    }
    
    /// function show image
    public func Glide(imageView1: UIImageView,urlImage: String){
        imageView1.sd_setImage(with: URL(string :urlImage))
    }
    
    /// function save value local
    public func setValueLocal(value:String, key:String) {
        let preferences = UserDefaults.standard
        let currentLevel = value
        let currentLevelKey = key
        preferences.set(currentLevel, forKey: currentLevelKey)
    }
    
    /// function   value local
    public func clearValueLocal(key:String){
        let preferences = UserDefaults.standard
        preferences.removeObject(forKey: key)
    }
    
    /// function get value local
    public func getValueLocal(key:String) -> String{
        let preferences = UserDefaults.standard
        let firstLogin = key
        if preferences.object(forKey: firstLogin) == nil {
            return "nil"
        }
        return preferences.object(forKey: firstLogin) as! String
    }
    
    /* MAKE: play sound online **/
    public func playSound(sound:String)  {
        let url = URL.init(string: sound)
        player = AVPlayer.init(url: url!)
        player.play()
    }
    
    /* MAKE: playAudioFromProject */
    public func playAudioFromProject(sound:String) {
        guard let url = Bundle.main.url(forResource: sound, withExtension: "mp3") else {
            return
        }
        do { audioPlayer = try AVAudioPlayer(contentsOf: url) }
        catch { print("audio file error") }
        audioPlayer?.play()
    }
}

extension UIView {
    enum Side {
        case top
        case bottom
        case left
        case right
    }

    func addBorder(to side: Side, color: UIColor, borderWidth: CGFloat) {
        let subLayer = CALayer()
        subLayer.borderColor = color.cgColor
        subLayer.borderWidth = borderWidth
        let origin = findOrigin(side: side, borderWidth: borderWidth)
        let size = findSize(side: side, borderWidth: borderWidth)
        subLayer.frame = CGRect(origin: origin, size: size)
        layer.addSublayer(subLayer)
    }

    private func findOrigin(side: Side, borderWidth: CGFloat) -> CGPoint {
        switch side {
        case .right:
            return CGPoint(x: frame.maxX - borderWidth, y: 0)
        case .bottom:
            return CGPoint(x: 0, y: frame.maxY - borderWidth)
        default:
            return .zero
        }
    }

    private func findSize(side: Side, borderWidth: CGFloat) -> CGSize {
        switch side {
        case .left, .right:
            return CGSize(width: borderWidth, height: frame.size.height)
        case .top, .bottom:
            return CGSize(width: frame.size.width, height: borderWidth)
        }
    }
    
    public func showToast(message : String) {
        let toastView = UILabel()
        toastView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        toastView.textColor = UIColor.white
        toastView.textAlignment = .center
        toastView.font = UIFont.preferredFont(forTextStyle: .caption1)
        toastView.layer.cornerRadius = 10
        toastView.layer.masksToBounds = true
        toastView.text = message
        toastView.numberOfLines = 0
        toastView.alpha = 0
        toastView.translatesAutoresizingMaskIntoConstraints = false

        let window = UIApplication.shared.keyWindowInConnectedScenes
        window?.addSubview(toastView)

        let horizontalCenterContraint: NSLayoutConstraint = NSLayoutConstraint(item: toastView, attribute: .centerX, relatedBy: .equal, toItem: window, attribute: .centerX, multiplier: 1, constant: 0)
        let widthContraint: NSLayoutConstraint = NSLayoutConstraint(item: toastView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 250)
        let verticalContraint: [NSLayoutConstraint] = NSLayoutConstraint.constraints(withVisualFormat: "V:|-(>=200)-[loginView(==40)]-60-|", options: [.alignAllCenterX, .alignAllCenterY], metrics: nil, views: ["loginView": toastView])

        NSLayoutConstraint.activate([horizontalCenterContraint, widthContraint])
        NSLayoutConstraint.activate(verticalContraint)

        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            toastView.alpha = 1
        }, completion: nil)

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.8, execute: {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
                toastView.alpha = 0
            }, completion: { finished in
                toastView.removeFromSuperview()
            })
        })
    }
}

