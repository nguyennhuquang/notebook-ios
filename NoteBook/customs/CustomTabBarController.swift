//
//  CustomTabBatVC.swift
//  uitabbartutorial
//
//  Created by Alexey Adamovsky on 05/10/2017.
//  Copyright © 2017 Alexey Adamovsky. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController, UITabBarControllerDelegate {

    static var shared:CustomTabBarController?
    private var _customTabBar:CustomTabBar!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    var customTabBar:CustomTabBar{
        get{
            return _customTabBar
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        //store shared property once while created CustomTabBarController
        if CustomTabBarController.shared == nil{
            CustomTabBarController.shared = self
        }
        
        //hide ios tabbar view
        self.tabBar.isHidden = true
        
        initViews()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - INITIALIZERS -
    private func initViews(){
        //init our custom view
        _customTabBar = CustomTabBar.getFromNib()
        
        //adding our custom tab bar to controller
        self.view.addSubview(customTabBar)
        
        //setting constrains to custom tab bar and actions of buttons
        setupTabBar()
    }
    private func setupTabBar(){
        _customTabBar.setActionHomeTapped {
            self.selectedIndex = 0
        }
        _customTabBar.setActionCartTapped {
            self.selectedIndex = 1
        }
        _customTabBar.setActionGraphTapped {
            self.selectedIndex = 2
        }
        setupTabBarAutoLayout()
    }
    private func setupTabBarAutoLayout() {
        _customTabBar.translatesAutoresizingMaskIntoConstraints = false
        _customTabBar.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        _customTabBar.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        _customTabBar.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

    }
    
    
    
    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return MyTransition(viewControllers: tabBarController.viewControllers)
    }

    class MyTransition: NSObject, UIViewControllerAnimatedTransitioning {

        let viewControllers: [UIViewController]?
        let transitionDuration: Double = 0.4

        init(viewControllers: [UIViewController]?) {
            self.viewControllers = viewControllers
        }

        func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
            return TimeInterval(transitionDuration)
        }

        func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

            guard
                let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
                let fromView = fromVC.view,
                let fromIndex = getIndex(forViewController: fromVC),
                let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to),
                let toView = toVC.view,
                let toIndex = getIndex(forViewController: toVC)
                else {
                    transitionContext.completeTransition(false)
                    return
            }

            let frame = transitionContext.initialFrame(for: fromVC)
            var fromFrameEnd = frame
            var toFrameStart = frame
            fromFrameEnd.origin.x = toIndex > fromIndex ? frame.origin.x - frame.width : frame.origin.x + frame.width
            toFrameStart.origin.x = toIndex > fromIndex ? frame.origin.x + frame.width : frame.origin.x - frame.width
            toView.frame = toFrameStart

            DispatchQueue.main.async {
                transitionContext.containerView.addSubview(toView)
                UIView.animate(withDuration: self.transitionDuration, animations: {
                    fromView.frame = fromFrameEnd
                    toView.frame = frame
                }, completion: {success in
                    fromView.removeFromSuperview()
                    transitionContext.completeTransition(success)
                })
            }
        }

        func getIndex(forViewController vc: UIViewController) -> Int? {
            guard let vcs = self.viewControllers else { return nil }
            for (index, thisVC) in vcs.enumerated() {
                if thisVC == vc { return index }
            }
            return nil
        }
    }


}
