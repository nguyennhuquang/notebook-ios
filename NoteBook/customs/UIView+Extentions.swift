//
//  UIView+Extentions.swift
//  LaziApp
//
//  Created by Manh on 10/9/18.
//  Copyright © 2018 Manh. All rights reserved.
//

import UIKit
import AVFoundation

//var player: AVPlayer!
//var audioPlayer : AVAudioPlayer!

extension UIView{
    
    /* MAKE: add shadow UIView **/
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float){
         layer.masksToBounds = false
         layer.shadowOffset = offset
         layer.shadowColor = color.cgColor
         layer.shadowRadius = radius
         layer.shadowOpacity = opacity
         let backgroundCGColor = backgroundColor?.cgColor
         backgroundColor = nil
         layer.backgroundColor =  backgroundCGColor
     }
    
    /* MAKE: add shadow black **/
    func addTopShadow()  {
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 6
    }
    
    /* MAKE: add shadow white **/
    func addWhiteShadow()  {
        self.layer.shadowColor = UIColor.white.cgColor
        self.layer.shadowOpacity = 1.0
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 6
    }
    
    /* MAKE: sizeToFitCustom **/
    func sizeToFitCustom () {
        var size = CGSize(width: self.frame.width, height: 0)
        for view in self.subviews {
            let frame = view.frame
            let newH = frame.origin.y + frame.height
            if newH > size.height {
                size.height = newH
            }
        }
        self.frame.size = size
    }
    
    /* MAKE: Gọi đến controller cha **/
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    /* MAKE: Save value local **/
    public func setValueLocal(value:String, key:String) {
        let preferences = UserDefaults.standard
        let currentLevel = value
        let currentLevelKey = key
        preferences.set(currentLevel, forKey: currentLevelKey)
    }
    
    /* MAKE: Delete value local **/
    public func clearValueLocal(key:String){
        let preferences = UserDefaults.standard
        preferences.removeObject(forKey: key)
    }
    
    /* MAKE: Get value local **/
    public func getValueLocal(key:String) -> String{
        let preferences = UserDefaults.standard
        let firstLogin = key
        if preferences.object(forKey: firstLogin) == nil {
            return "nil"
        }
        return preferences.object(forKey: firstLogin) as! String
    }
    
//    /* MAKE: play sound online **/
//    public func playSound(sound:String)  {
//        let url = URL.init(string: sound)
//        player = AVPlayer.init(url: url!)
//        player.play()
//    }
//
//    /* MAKE: playAudioFromProject */
//    public func playAudioFromProject(sound:String) {
//        guard let url = Bundle.main.url(forResource: sound, withExtension: "mp3") else {
//            return
//        }
//        do { audioPlayer = try AVAudioPlayer(contentsOf: url) }
//        catch { print("audio file error") }
//        audioPlayer?.play()
//    }
    
}


extension CALayer {
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: thickness)
            
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: self.bounds.height - thickness,  width: UIScreen.main.bounds.width, height: thickness)
            
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0,  width: thickness, height: self.bounds.height)
        default:
             border.frame = CGRect(x: self.bounds.width - thickness, y: 0,  width: thickness, height: self.bounds.height)
        }
        border.backgroundColor = color.cgColor;
        self.addSublayer(border)
    }
}

extension UIImage {
    func croppedInRect(rect: CGRect) -> UIImage {
        func rad(_ degree: Double) -> CGFloat {
            return CGFloat(degree / 180.0 * .pi)
        }
        
        var rectTransform: CGAffineTransform
        switch imageOrientation {
        case .left:
            rectTransform = CGAffineTransform(rotationAngle: rad(90)).translatedBy(x: 0, y: -self.size.height)
        case .right:
            rectTransform = CGAffineTransform(rotationAngle: rad(-90)).translatedBy(x: -self.size.width, y: 0)
        case .down:
            rectTransform = CGAffineTransform(rotationAngle: rad(-180)).translatedBy(x: -self.size.width, y: -self.size.height)
        default:
            rectTransform = .identity
        }
        rectTransform = rectTransform.scaledBy(x: self.scale, y: self.scale)
        
        let imageRef = self.cgImage!.cropping(to: rect.applying(rectTransform))
        let result = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
        return result
    }
}
