//
//  UIView.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 12/25/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit
import AVFoundation

var player: AVPlayer!
var audioPlayer : AVAudioPlayer!

extension UIView{
    
    /* MAKE: add shadow UIView **/
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float){
         layer.masksToBounds = false
         layer.shadowOffset = offset
         layer.shadowColor = color.cgColor
         layer.shadowRadius = radius
         layer.shadowOpacity = opacity
         let backgroundCGColor = backgroundColor?.cgColor
         backgroundColor = nil
         layer.backgroundColor =  backgroundCGColor
     }
    
    /* MAKE: add shadow black **/
    func addTopShadow()  {
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 6
    }
    
    /* MAKE: add shadow white **/
    func addWhiteShadow()  {
        self.layer.shadowColor = UIColor.white.cgColor
        self.layer.shadowOpacity = 1.0
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 6
    }
    
    /* MAKE: sizeToFitCustom **/
    func sizeToFitCustom () {
        var size = CGSize(width: self.frame.width, height: 0)
        for view in self.subviews {
            let frame = view.frame
            let newH = frame.origin.y + frame.height
            if newH > size.height {
                size.height = newH
            }
        }
        self.frame.size = size
    }
    
    /* MAKE: show popup check true false **/
    func showPopUp(background:String, english:String, translate:String, spelling:String, row:Int) {
        let blackView = UIView()
        let contentView = (Bundle.main.loadNibNamed("CustomSlidePadingBottom", owner: self, options: nil)?.first as? CustomSlidePadingBottom)!
        contentView.tag = 98
        blackView.tag = 99
        if let window = UIApplication.shared.keyWindowInConnectedScenes {
            
            let xForm: CGFloat = 15
            let yForm: CGFloat = window.frame.height
            let width: CGFloat = window.frame.width - 30
            let height:CGFloat =  220
            let xTo: CGFloat =  15
            let yTo: CGFloat = window.frame.height - 240
            
            window.addSubview(blackView)
            window.addSubview(contentView)
            blackView.frame = window.frame
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.6)
            blackView.alpha = 0
            
            contentView.alpha = 1
            contentView.backgroundColor = UIColor(hexString: background)!
            contentView.txtTranslate.text = english
            contentView.txtEngish.text = translate
            contentView.txtSpell.text = "/" + spelling + "/"
            contentView.layer.cornerRadius = 10
            contentView.frame = CGRect(x: xForm, y: yForm, width: width , height: height)
            contentView.actionbtn.addTarget(self, action: #selector(scrollPage1), for: .touchUpInside)
            contentView.actionbtn.tag = row
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                blackView.alpha = 1
                contentView.frame = CGRect(x: xTo, y: yTo, width: width, height: height)
            }, completion: nil)
            
        }
    }
    
    // MAKE: hàm chuyển câu hỏi
    @objc func scrollPage1(button: UIButton) {
        let window = UIApplication.shared.keyWindowInConnectedScenes
        for subview in window!.subviews {
            if (subview.tag == 99 || subview.tag == 98) {
                subview.removeFromSuperview()
            }
        }
        let parent = self.parentViewController as! QuestionViewController
        let row = button.tag
        if row < parent.questionCustom.count - 1 {
            let indexPath = NSIndexPath(row: row + 1, section: 0)
            parent.collectionQuestion.scrollToItem(at: indexPath as IndexPath, at: .centeredHorizontally, animated: true)
            parent.widthProgressBar.constant = parent.widthProgressBar.constant + (parent.progressBar.frame.size.width / CGFloat(parent.questionCustom.count))
        }else{
            print("success")
            parent.successNavigation()
        }

    }
    
    /* MAKE: Gọi đến controller cha **/
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    /* MAKE: Save value local **/
    public func setValueLocal(value:String, key:String) {
        let preferences = UserDefaults.standard
        let currentLevel = value
        let currentLevelKey = key
        preferences.set(currentLevel, forKey: currentLevelKey)
    }
    
    /* MAKE: Delete value local **/
    public func clearValueLocal(key:String){
        let preferences = UserDefaults.standard
        preferences.removeObject(forKey: key)
    }
    
    /* MAKE: Get value local **/
    public func getValueLocal(key:String) -> String{
        let preferences = UserDefaults.standard
        let firstLogin = key
        if preferences.object(forKey: firstLogin) == nil {
            return "nil"
        }
        return preferences.object(forKey: firstLogin) as! String
    }
    
    /* MAKE: play sound online **/
    public func playSound(sound:String)  {
        let url = URL.init(string: sound)
        player = AVPlayer.init(url: url!)
        player.play()
    }
    
    /* MAKE: playAudioFromProject */
    public func playAudioFromProject(sound:String) {
        guard let url = Bundle.main.url(forResource: sound, withExtension: "mp3") else {
            return
        }
        do { audioPlayer = try AVAudioPlayer(contentsOf: url) }
        catch { print("audio file error") }
        audioPlayer?.play()
    }
    
    /* MAKE: Toast Show bottom */
    public func showToast(message : String) {
        let toastView = UILabel()
        toastView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        toastView.textColor = UIColor.white
        toastView.textAlignment = .center
        toastView.font = UIFont.preferredFont(forTextStyle: .caption1)
        toastView.layer.cornerRadius = 10
        toastView.layer.masksToBounds = true
        toastView.text = message
        toastView.numberOfLines = 0
        toastView.alpha = 0
        toastView.translatesAutoresizingMaskIntoConstraints = false

        let window = UIApplication.shared.keyWindowInConnectedScenes
        window?.addSubview(toastView)

        let horizontalCenterContraint: NSLayoutConstraint = NSLayoutConstraint(item: toastView, attribute: .centerX, relatedBy: .equal, toItem: window, attribute: .centerX, multiplier: 1, constant: 0)
        let widthContraint: NSLayoutConstraint = NSLayoutConstraint(item: toastView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 250)
        let verticalContraint: [NSLayoutConstraint] = NSLayoutConstraint.constraints(withVisualFormat: "V:|-(>=200)-[loginView(==40)]-60-|", options: [.alignAllCenterX, .alignAllCenterY], metrics: nil, views: ["loginView": toastView])

        NSLayoutConstraint.activate([horizontalCenterContraint, widthContraint])
        NSLayoutConstraint.activate(verticalContraint)

        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            toastView.alpha = 1
        }, completion: nil)

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.8, execute: {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
                toastView.alpha = 0
            }, completion: { finished in
                toastView.removeFromSuperview()
            })
        })
    }
}


extension CALayer {
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: thickness)
            
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: self.bounds.height - thickness,  width: UIScreen.main.bounds.width, height: thickness)
            
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0,  width: thickness, height: self.bounds.height)
        default:
             border.frame = CGRect(x: self.bounds.width - thickness, y: 0,  width: thickness, height: self.bounds.height)
        }
        border.backgroundColor = color.cgColor;
        self.addSublayer(border)
    }
}

extension UIImage {
    func croppedInRect(rect: CGRect) -> UIImage {
        func rad(_ degree: Double) -> CGFloat {
            return CGFloat(degree / 180.0 * .pi)
        }
        
        var rectTransform: CGAffineTransform
        switch imageOrientation {
        case .left:
            rectTransform = CGAffineTransform(rotationAngle: rad(90)).translatedBy(x: 0, y: -self.size.height)
        case .right:
            rectTransform = CGAffineTransform(rotationAngle: rad(-90)).translatedBy(x: -self.size.width, y: 0)
        case .down:
            rectTransform = CGAffineTransform(rotationAngle: rad(-180)).translatedBy(x: -self.size.width, y: -self.size.height)
        default:
            rectTransform = .identity
        }
        rectTransform = rectTransform.scaledBy(x: self.scale, y: self.scale)
        
        let imageRef = self.cgImage!.cropping(to: rect.applying(rectTransform))
        let result = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
        return result
    }
}

extension UILabel {
    func setLineHeight(lineHeight: CGFloat) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1.0
        paragraphStyle.lineHeightMultiple = lineHeight
        paragraphStyle.alignment = self.textAlignment

        let attrString = NSMutableAttributedString()
        if (self.attributedText != nil) {
            attrString.append( self.attributedText!)
        } else {
            attrString.append( NSMutableAttributedString(string: self.text!))
            attrString.addAttribute(NSAttributedString.Key.font, value: self.font!, range: NSMakeRange(0, attrString.length))
        }
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
        self.attributedText = attrString
    }
}

