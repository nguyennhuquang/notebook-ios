//
//  CustomTabBar.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/22/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

class CustomTabBar: UIView {

    private var actionHomeTapped:()->() = {}
    private var actionCartTapped:()->() = {}
    private var actionGraphTapped:()->() = {}

    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var btnGraph: UIButton!
//    @IBOutlet weak var badgeView: UIView!
//    @IBOutlet weak var lblBadgeCount: UILabel!
    
    
    static func getFromNib()->CustomTabBar{
        let tabBarView = UINib.init(nibName: "CustomTabBar", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomTabBar
        tabBarView.layer.shadowColor = #colorLiteral(red: 0.8979414105, green: 0.8980956078, blue: 0.8979316354, alpha: 1)
        tabBarView.layer.shadowOpacity = 1
        tabBarView.layer.masksToBounds =  false
        return tabBarView
    }
    
    override func layoutSubviews() {
        self.btnHome.roundCorners(corners: [.topLeft], size: 20)
        self.btnGraph.roundCorners(corners: [.topRight], size: 20)
    }
    
    @IBAction func btnHomeTapped(_ sender: Any) {
        //change images to unselected to all buttons
        setAllButtonsUnselected()
        //set image selected
        btnHome.setImage(UIImage.init(named: "home-selected"), for: .normal)
        actionHomeTapped()
    }
    
    @IBAction func btnCartTapped(_ sender: Any) {
        setAllButtonsUnselected()
        btnCart.setImage(UIImage.init(named: "cart-selected"), for: .normal)
        actionCartTapped()
    }
    
    @IBAction func btnGraphTapped(_ sender: Any) {
        setAllButtonsUnselected()
        btnGraph.setImage(UIImage.init(named: "graph-selected"), for: .normal)
        actionGraphTapped()
    }

    
    //MARK: - VIEW SETTERS -
    func setActionHomeTapped(action:@escaping ()->()){
        actionHomeTapped = action
    }
    func setActionCartTapped(action:@escaping ()->()){
        actionCartTapped = action
    }
    func setActionGraphTapped(action:@escaping ()->()){
        actionGraphTapped = action
    }

//    func setBadgeNum(num:Int){
//
//        if num > 0{//set number before badge appeared
//            self.lblBadgeCount.text = "\(num)"
//        }
//        //hide badge view if badge is 0
//        UIView.animate(withDuration: 0.3, animations: {
//            if num == 0{
//                self.badgeView.alpha = 0
//            }
//            else{
//                self.badgeView.alpha = 1
//            }
//        }) { (true) in
//            //set number after badge disappeared
//            //importand when badge is 0
//            self.lblBadgeCount.text = "\(num)"
//        }
//    }
//
//    //MARK: - VIEW GETTERS -
//    func getBadgeNum()->Int{
//         return Int("\(lblBadgeCount.text!)")!
//    }
    
    //MARK: - VIEW FUNCTIONS -
    func setAllButtonsUnselected(){
        btnHome.setImage(UIImage.init(named: "home"), for: .normal)
        btnCart.setImage(UIImage.init(named: "cart"), for: .normal)
        btnGraph.setImage(UIImage.init(named: "graph"), for: .normal)
    }
    
}

extension UIView{
    public func setCornerRadius(_ radius:CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    func roundCorners(corners:UIRectCorner, size:CGFloat){
        let path = UIBezierPath(roundedRect:self.bounds,
                                byRoundingCorners:corners,
                                cornerRadii: CGSize(width: size, height:  size))
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
}

