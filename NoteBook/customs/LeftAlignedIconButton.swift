//
//  LeftAlignedIconButton.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/22/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

@IBDesignable
class LeftAlignedIconButton: UIButton {
    override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
        let titleRect = super.titleRect(forContentRect: contentRect)
        let imageSize = currentImage?.size ?? .zero
        let availableWidth = contentRect.width - imageEdgeInsets.right - imageSize.width - titleRect.width
        return titleRect.offsetBy(dx: round(availableWidth / 2), dy: 0)
    }
}
