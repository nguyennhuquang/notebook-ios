//
//  setBorderRadius.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/22/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    public func setCornerRadius(_ radius:CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    func roundCorners(corners:UIRectCorner, size:CGFloat){
        let path = UIBezierPath(roundedRect:self.bounds,
                                byRoundingCorners:corners,
                                cornerRadii: CGSize(width: size, height:  size))
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
}
