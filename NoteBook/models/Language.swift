//
//  Language.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/14/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import Foundation

class Language {
    var name: String?
    var icon:String?
    var image:String?
    var code: String?
    
    init(name:String, icon:String, image:String, code:String) {
        self.name = name
        self.icon = icon
        self.image = image
        self.code = code
    }
    
}
