//
//  Search.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 12/23/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import Foundation
import UIKit
struct Card {
    var name:String =  ""
    var image:String = ""
    var audio:String = ""
}

class CardModel {
    var cardList = NSArray()
    var searchResult = NSArray()
    var searchMode = Bool()
    
    init() {
        cardList = []
        searchResult = []
        searchMode = false
        readDataFormList()
    }
    
    private func readDataFormList(){
        cardList = [
            Card(name: "A", image: "", audio: ""),
            Card(name: "B", image: "", audio: ""),
            Card(name: "C", image: "", audio: ""),
            Card(name: "D", image: "", audio: ""),
            Card(name: "E", image: "", audio: ""),
            Card(name: "F", image: "", audio: ""),
            Card(name: "G", image: "", audio: ""),
        ]
    }

    public func searchWithText(keyword:String){
        if (keyword.isEmpty) {
            clearSearch()
        }else{
            searchMode = true
            let predict = NSPredicate(format: "SELF['@name'] CONTAINS[c] %@", keyword)
            searchResult = cardList.filtered(using: predict) as NSArray
        }
    }

    public func clearSearch() {
        searchMode = false
        searchResult = []
    }
    
    public func numberOfItem() -> Int {
        if searchMode {
            return searchResult.count
        }else{
            return cardList.count
        }
    }
    
    public func getItemAtIndex(index:Int) -> Card {
        var cardObj:Card = Card()
        var cardDict = cardList.object(at: index)
        if (searchMode) {
            cardDict = searchResult.object(at: index)
        }
        
        cardObj.name = cardDict["@name"] as! String
        cardObj.image = cardDict["@image"] as! String
        cardObj.name = cardDict["@audio"] as! String
        
        return cardObj
    }
    
}

