//
//  Favorite.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/28/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import Foundation
import UIKit

class Favorite {
    var id:Int16?
    var name:String?
    var image:Data?
    var isExpanded = false    // Bool to determine whether the cell is expanded
    var classify: String?
    var phrases:[PhraseFavoriteData] = []
    
    init(id:Int16 ,name:String, image:Data, isExpanded:Bool, classify:String ,phrases:[PhraseFavoriteData]) {
        self.name = name
        self.image = image
        self.isExpanded = isExpanded
        self.classify = classify
        self.phrases = phrases
        self.id = id
    }
}

class PhraseFavoriteData {
    var id:Int16?
    var category_id:Int16?
    var opened:Bool
    var name: String?
    var status: Int16?
    var translate: String?
    var sound: Data?
    
    init(opened:Bool, name:String, status:Int16, translate:String, sound:Data, id:Int16, category_id:Int16) {
        self.opened = opened
        self.name = name
        self.status = status
        self.translate = translate
        self.sound = sound
        self.id = id
        self.category_id = category_id
    }
}
