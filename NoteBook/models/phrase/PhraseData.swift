//
//  PhraseData.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/23/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import Foundation

class PhraseData {
    var id:Int16?
    var opened:Bool
    var name: String?
    var status: Int16?
    var translate: String?
    var sound: Data?
    
    init(opened:Bool, name:String, status:Int16, translate:String, sound:Data, id:Int16) {
        self.opened = opened
        self.name = name
        self.status = status
        self.translate = translate
        self.sound = sound
        self.id = id
    } 
}
