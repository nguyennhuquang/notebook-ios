//
//  GrammarTableViewCell.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/26/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit
import CoreData

@available(iOS 13.0, *)
class GrammarTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txtNameGrammar: UILabel!
    @IBOutlet weak var actionFavorite: UIButton!
    var statusFavorite:Int16 = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func Favorite(_ sender: Any) {
        let idGrammar = (sender as AnyObject).tag as Int
        if statusFavorite == 0{
            statusFavorite = 1
            actionFavorite.setImage(UIImage(named: "favorite-select"), for: .normal)
            onUpdateFavorite(idGrammar: idGrammar, status: 1)
            self.showToast(message: "Đã thêm vào mục yêu thích")
        }else{
            statusFavorite = 0
            actionFavorite.setImage(UIImage(named: "favorite"), for: .normal)
            onUpdateFavorite(idGrammar: idGrammar, status: 0)
            self.showToast(message: "Đã gỡ khỏi mục yêu thích")
        }
    }
    
    /// action uơdate status phrase
    func onUpdateFavorite(idGrammar:Int,status:Int16){
      guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
      let managedContext = appDelegate.persistentContainer.viewContext
      let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "GrammarCR")
      fetchRequest.predicate = NSPredicate(format: "id = %@", String(idGrammar))
      do{
        let test = try managedContext.fetch(fetchRequest)
        let objectUpdate = test[0] as! NSManagedObject
        objectUpdate.setValue(status, forKey: "status")
         do{
            try managedContext.save()
         }
         catch
         {print(error)}
       }
      catch
      {print(error)}
    }
}
