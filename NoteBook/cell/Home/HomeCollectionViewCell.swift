//
//  HomeCollectionViewCell.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/21/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageCtegory: UIImageView!
    @IBOutlet weak var nameCategory: UILabel!
    @IBOutlet weak var borderRight: UIView!
    @IBOutlet weak var borderBottom: UIView!
    @IBOutlet weak var borderLeft: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setNameCategory(name:String) {
        nameCategory.text = name
    }
}

    


