//
//  LanguageTableView.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/19/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

class LanguageTableView: UITableViewCell {
    @IBOutlet weak var backgroundLanguage: UIImageView!
    @IBOutlet weak var iconLanguage: UIImageView!
    @IBOutlet weak var nameLanguage: UILabel!
    @IBOutlet weak var activeLanguage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override open var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame =  newFrame
            frame.origin.y += 25
            super.frame = frame
        }
    }
    
    func setBackgroundLanguage(nameImage:String) {
        backgroundLanguage.image = UIImage(named: nameImage)
    }
    
    func setIconLanguage(nameIcon:String) {
        iconLanguage.image = UIImage(named: nameIcon)
    }
    
    func setNameLanguage(name:String) {
        nameLanguage.text = name
    }
    
}
