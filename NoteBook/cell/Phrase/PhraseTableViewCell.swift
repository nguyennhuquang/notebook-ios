//
//  PhraseTableViewCell.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/22/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit
import AVFoundation

class PhraseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ghiam: CustomButton!
    @IBOutlet weak var txtTranslate: UILabel!
    @IBOutlet weak var playSoundPhrase: CustomButton!
    @IBOutlet weak var txtSpeed: CustomButton!
    var numberSelectedSpeed:Int = 0
    var urlSound:Data?
    var player:AVAudioPlayer!
    var arrSpeed = ["1x","0.75","0.5","0.25"]
    var arrValueSpeed = [1.0,0.75,0.5,0.25]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        do{
            let data = urlSound!
            player = try AVAudioPlayer(data: data as Data)
            player.enableRate = true
        }
        catch{
            print("err")
        }
    }
    var timer:Timer!
    @IBAction func play(_ sender: Any) {
        player.play()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(checkTime), userInfo: nil, repeats: true)
        playSoundPhrase.backgroundColor = #colorLiteral(red: 0.7632548213, green: 0.8162221313, blue: 0.7998405695, alpha: 1)
    }
    
    @objc func checkTime() {
        if player.currentTime == 0 {
            playSoundPhrase.backgroundColor = #colorLiteral(red: 0.8317679763, green: 0.9580102563, blue: 0.9213493466, alpha: 1)
        }
    }
    
    @IBAction func selectedSpeed(_ sender: Any) {
        numberSelectedSpeed = numberSelectedSpeed + 1
        if numberSelectedSpeed < arrSpeed.count {
            txtSpeed.setTitle(arrSpeed[numberSelectedSpeed], for: .normal)
            player.rate = Float(arrValueSpeed[numberSelectedSpeed])
        }else{
            numberSelectedSpeed = 0
            txtSpeed.setTitle(arrSpeed[numberSelectedSpeed], for: .normal)
            player.rate = Float(arrValueSpeed[numberSelectedSpeed])
        }
    }
}
