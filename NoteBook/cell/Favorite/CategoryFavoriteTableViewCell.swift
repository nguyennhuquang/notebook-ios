//
//  CategoryFavoriteTableViewCell.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/29/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit
import AVFoundation

class CategoryFavoriteTableViewCell: UITableViewCell {

    var urlSound:Data?
    var player:AVAudioPlayer!
    var arrSpeed = ["1x","0.75","0.5","0.25"]
    var arrValueSpeed = [1.0,0.75,0.5,0.25]
    var numberSelectedSpeed:Int = 0
    @IBOutlet weak var actionSHowHideView: UIButton!
    @IBOutlet weak var txtPhraseName: UILabel!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var contentPhrase: UIView!
    @IBOutlet weak var borderHeader: UIView!
    @IBOutlet weak var borderFoot: UIView!
    @IBOutlet weak var txtTranslate: UILabel!
    @IBOutlet weak var heightContentPhrase: NSLayoutConstraint!
    var heightTranslate:CGFloat = 0
    @IBOutlet weak var txtSpeed: CustomButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func closeCell() {
        heightContentPhrase.constant = 0
        contentPhrase.setNeedsLayout()
    }
    
    func openCell() {
        self.setNeedsUpdateConstraints()
        self.setNeedsLayout()
        self.layoutIfNeeded()
        heightContentPhrase.constant = 16 + 40 + 16 + txtTranslate.frame.size.height
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        do{
            let data = urlSound!
            player = try AVAudioPlayer(data: data as Data)
            player.enableRate = true
        }
        catch{
            print("err")
        }
    }
    
    @IBAction func actionFavorite(_ sender: Any) {
    
    }
    
    @IBAction func onShowHideContentPhrase(_ sender: Any) {
        if heightContentPhrase.constant == 0{
            heightContentPhrase.constant = 16 + 40 + 16 + txtTranslate.frame.size.height
            heightTranslate = txtTranslate.frame.size.height
        }else{
            heightContentPhrase.constant = 0
        }
        self.setNeedsUpdateConstraints()
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    @IBAction func playSound(_ sender: Any) {
        player.enableRate = true
        player.play()
    }
    
    @IBAction func speedSound(_ sender: Any) {
        numberSelectedSpeed = numberSelectedSpeed + 1
        if numberSelectedSpeed < arrSpeed.count {
            txtSpeed.setTitle(arrSpeed[numberSelectedSpeed], for: .normal)
            player.rate = Float(arrValueSpeed[numberSelectedSpeed])
        }else{
            numberSelectedSpeed = 0
            txtSpeed.setTitle(arrSpeed[numberSelectedSpeed], for: .normal)
            player.rate = Float(arrValueSpeed[numberSelectedSpeed])
        }
    }
}
