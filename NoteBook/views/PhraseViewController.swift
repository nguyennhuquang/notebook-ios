//
//  PhraseViewController.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/22/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit
import CoreData

@available(iOS 13.0, *)
class PhraseViewController: UIViewController {
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .darkContent
//    }
    
    var DataResponse:[PhraseData] = []
    var phraseCoreData: [NSManagedObject] = []
    var translateCoreData: [NSManagedObject] = []
    
    @IBOutlet weak var backToPre: UIButton!
    @IBOutlet weak var PhraseTableView: UITableView!
    @IBOutlet weak var viewHeaderPhrase: UIView!
    @IBOutlet var viewPhrase: UIView!
    @IBOutlet weak var containerTableView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PhraseTableView.dataSource = self
        PhraseTableView.delegate = self
        PhraseTableView.register(UINib(nibName: "PhraseTableViewCell", bundle: nil), forCellReuseIdentifier: "PhraseTableViewCell")
        PhraseTableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        backToPre.setTitle(" " + " " + self.getValueLocal(key: "category_name"), for: .normal)
        setupColor()
        getDataPhraseFormCoreData()
        PhraseTableView.separatorStyle = .none
    }
    
    func setupColor()  {
        let background = self.getValueLocal(key: "color-background")
        viewHeaderPhrase.backgroundColor = UIColor(hexString: background)!
        viewPhrase.backgroundColor = UIColor(hexString: background)!
        PhraseTableView.backgroundColor = UIColor(hexString: "#FFFFFF")!
        containerTableView.backgroundColor = UIColor(hexString: "#FFFFFF")!
    }
    

    @IBAction func onBackViewPre(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    ///get data Phrase  form core data
    func getDataPhraseFormCoreData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
          return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestPhrase = NSFetchRequest<NSManagedObject>(entityName: "PhraseCR")
        let fetchRequestTranslate = NSFetchRequest<NSManagedObject>(entityName: "TranslateCR")
        do {
          phraseCoreData = try managedContext.fetch(fetchRequestPhrase)
          for item in phraseCoreData{
            do {
              translateCoreData = try managedContext.fetch(fetchRequestTranslate)
                for itemTranslate in translateCoreData {
                    if itemTranslate.value(forKey: "phrase_id") as! Int16 == item.value(forKey: "id") as! Int16 && itemTranslate.value(forKey: "language_code") as! String == self.getValueLocal(key: "language_code") && (item.value(forKey: "category_id") as! Int16) ==  Int16(self.getValueLocal(key: "category_id")){
                        
                        DataResponse.append(PhraseData(opened: false, name: item.value(forKey: "name") as! String, status: item.value(forKey: "status") as! Int16, translate: itemTranslate.value(forKey: "translate") as! String, sound: itemTranslate.value(forKey: "sound") as! Data, id: item.value(forKey: "id") as! Int16))
                    }else{
                    }
                }
            } catch let error as NSError {
              print("Could not fetch. \(error), \(error.userInfo)")
            }
          }
          self.PhraseTableView.reloadData()
        } catch let error as NSError {
          print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}


@available(iOS 13.0, *)
extension PhraseViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return DataResponse.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !DataResponse[section].opened {
            return 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = PhraseTableView.dequeueReusableCell(withIdentifier: "PhraseTableViewCell") as! PhraseTableViewCell
        cell.txtTranslate.text = DataResponse[indexPath.section].translate
        cell.urlSound = DataResponse[indexPath.section].sound
        cell.ghiam.isHidden = true
        return cell
    }
}

@available(iOS 13.0, *)
@available(iOS 13.0, *)
extension PhraseViewController: UITableViewDelegate{

    /// add viewHeader to table view
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = Bundle.main.loadNibNamed("viewForHeader", owner: self, options: nil)?.first as! viewForHeader
        headerCell.statusFavorite = DataResponse[section].status!
        headerCell.actionView.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
        headerCell.actionView.tag = section
        headerCell.actionFavorite.tag = section
        headerCell.actionFavorite.addTarget(self, action: #selector(onFavoriteFhrase), for: .touchUpInside)
        if DataResponse[section].status == 1{
            headerCell.actionFavorite.setImage(UIImage(named: "favorite-select"), for: .normal)
        }else{
            headerCell.actionFavorite.setImage(UIImage(named: "favorite"), for: .normal)
        }
        headerCell.txtNamePhrase.text = DataResponse[section].name!
        let str : String = self.getValueLocal(key: "font-size")
        let secStr : NSString = str as NSString
        let fontSize : CGFloat = CGFloat(secStr.doubleValue)
        headerCell.txtNamePhrase.font = UIFont(name: "Cabin-Medium", size: fontSize)
        return headerCell
    }
    
    ///add view footer to table view
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = #colorLiteral(red: 0.8317679763, green: 0.9580102563, blue: 0.9213493466, alpha: 1)
        return footerView
    }
    
    ///set height view footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    ///action show hide view
    @objc func handleExpandClose(button: UIButton) {
        let section = button.tag
        var indexPaths = [IndexPath]()
        let indexPath = IndexPath(row: 0, section: section)
        indexPaths.append(indexPath)
        let isExpanded = DataResponse[section].opened
        DataResponse[section].opened = !isExpanded
        if isExpanded {
            PhraseTableView.deleteRows(at: indexPaths, with: .fade)
        } else {
            PhraseTableView.insertRows(at: indexPaths, with: .fade)
        }
    }
    
    /// action uơdate status phrase
    @objc func onFavoriteFhrase(section:UIButton){
      var status:Int = 0
      let idPhrase = String(DataResponse[section.tag].id!)
      if DataResponse[section.tag].status! == 0 {
          DataResponse[section.tag].status! = 1
          status = 1
          self.showToast(message: "Đã thêm vào mục yêu thích")
      }else{
          DataResponse[section.tag].status! = 0
          status = 0
          self.showToast(message: "Đã gỡ khỏi mục yêu thích")
      }
      guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
      let managedContext = appDelegate.persistentContainer.viewContext
      let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "PhraseCR")
      fetchRequest.predicate = NSPredicate(format: "id = %@", idPhrase)
      do{
        let test = try managedContext.fetch(fetchRequest)
        let objectUpdate = test[0] as! NSManagedObject
        objectUpdate.setValue(status, forKey: "status")
         do{
            try managedContext.save()
         }
         catch
         {print(error)}
       }
      catch
      {print(error)}
    }
}
