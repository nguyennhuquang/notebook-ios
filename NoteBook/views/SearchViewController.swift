//
//  SearchViewController.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 12/16/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit
import CoreData

class demo {
    var name:String?
    var title:String?
    init(name:String, title:String) {
        self.name = name
        self.title = title
    }
}

@available(iOS 13.0, *)
@available(iOS 13.0, *)
class SearchViewController: UIViewController {
    
     var DataResponse:[PhraseData] = []
     var DataAll:[PhraseData] = []
     var phraseCoreData: [NSManagedObject] = []
     var translateCoreData: [NSManagedObject] = []
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .darkContent
//    }
    
    var searching = false
    
    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var borderBottom: UIView!
    @IBOutlet weak var headerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTable.delegate = self
        searchTable.dataSource = self
        searchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.barStyle = .black
        searchTable.register(UINib(nibName: "PhraseTableViewCell", bundle: nil), forCellReuseIdentifier: "PhraseTableViewCell")
//        searchBar.barTintColor = UIColor(hexString: "#F6F6F6")!
        searchTable.showsHorizontalScrollIndicator = false
        searchTable.showsVerticalScrollIndicator = false
        searchTable.estimatedSectionHeaderHeight = UITableView.automaticDimension
        searchTable.separatorStyle = .none
        setupColor()
        getAllData()
    }
    
    func setupColor() {
        let background = self.getValueLocal(key: "color-background")
        containerView.backgroundColor = UIColor(hexString: background)!
        headerView.backgroundColor = UIColor(hexString: background)!
        contentView.backgroundColor =  UIColor(hexString: background)!
        searchTable.backgroundColor =  UIColor(hexString: background)!
//        searchBar.backgroundColor =  UIColor(hexString: "#FFFFFF")!
    }
    
    func getDataSearch(string:String) {
        DataResponse = []
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "PhraseCR")
        let fetchRequestTranslate = NSFetchRequest<NSManagedObject>(entityName: "TranslateCR")
        fetchRequest.predicate = NSPredicate(format: "name CONTAINS[c] %@", string)
        do{
            phraseCoreData = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            for item in phraseCoreData{
              do {
                translateCoreData = try managedContext.fetch(fetchRequestTranslate)
                for itemTranslate in translateCoreData {
                    if itemTranslate.value(forKey: "phrase_id") as! Int16 == item.value(forKey: "id") as! Int16 &&
                       itemTranslate.value(forKey: "language_code") as! String == self.getValueLocal(key: "language_code") {
                       DataResponse.append(PhraseData(
                          opened: false,
                          name: item.value(forKey: "name") as! String,
                          status: item.value(forKey: "status") as! Int16,
                          translate: itemTranslate.value(forKey: "translate") as! String,
                          sound: itemTranslate.value(forKey: "sound") as! Data,
                          id: item.value(forKey: "id") as! Int16
                       ))
                    }else{}
                }
              } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
              }
            }
           do{ try managedContext.save() }
           catch { print(error)}
        }
        catch {print(error)}
     }
    
    func getAllData() {
        DataResponse = []
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "PhraseCR")
        let fetchRequestTranslate = NSFetchRequest<NSManagedObject>(entityName: "TranslateCR")
        do{
            phraseCoreData = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            for item in phraseCoreData{
              do {
                translateCoreData = try managedContext.fetch(fetchRequestTranslate)
                for itemTranslate in translateCoreData {
                    if itemTranslate.value(forKey: "phrase_id") as! Int16 == item.value(forKey: "id") as! Int16 &&
                       itemTranslate.value(forKey: "language_code") as! String == self.getValueLocal(key: "language_code") {
                       DataResponse.append(PhraseData(
                          opened: false,
                          name: item.value(forKey: "name") as! String,
                          status: item.value(forKey: "status") as! Int16,
                          translate: itemTranslate.value(forKey: "translate") as! String,
                          sound: itemTranslate.value(forKey: "sound") as! Data,
                          id: item.value(forKey: "id") as! Int16
                       ))
                    }else{}
                }
              } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
              }
            }
           do{ try managedContext.save() }
           catch { print(error)}
        }
        catch {print(error)}
     }
    
     @IBAction func actionBackToPre(_ sender: Any) {
        navigationController?.popViewController(animated: true)
     }
    
}

@available(iOS 13.0, *)
extension SearchViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return DataResponse.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !DataResponse[section].opened {
            return 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = searchTable.dequeueReusableCell(withIdentifier: "PhraseTableViewCell") as! PhraseTableViewCell
        cell.backgroundColor = UIColor(hexString: "#F6F6F6")!
        cell.txtTranslate.text = DataResponse[indexPath.section].translate
        cell.urlSound = DataResponse[indexPath.section].sound
        return cell
    }
    
}

@available(iOS 13.0, *)
@available(iOS 13.0, *)
extension SearchViewController: UITableViewDelegate {
    // MAKE: add viewHeader to table view
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = Bundle.main.loadNibNamed("viewForHeader", owner: self, options: nil)?.first as! viewForHeader
        headerCell.statusFavorite = DataResponse[section].status!
        headerCell.actionView.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
        headerCell.actionView.tag = section
        headerCell.actionFavorite.tag = section
        headerCell.actionFavorite.addTarget(self, action: #selector(onFavoriteFhrase), for: .touchUpInside)
        if DataResponse[section].status == 1{
            headerCell.actionFavorite.setImage(UIImage(named: "favorite-select"), for: .normal)
        }else{
            headerCell.actionFavorite.setImage(UIImage(named: "favorite"), for: .normal)
        }
        headerCell.txtNamePhrase.text = DataResponse[section].name!
        return headerCell
    }
    
    // MAKE: set height viewHeader
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // MAKE: add view footer to table view
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = #colorLiteral(red: 0.8317679763, green: 0.9580102563, blue: 0.9213493466, alpha: 1)
        return footerView
    }
    
    // MAKE: set height view footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    // MAKE: action show hide view
    @objc func handleExpandClose(button: UIButton) {
        let section = button.tag
        var indexPaths = [IndexPath]()
        let indexPath = IndexPath(row: 0, section: section)
        indexPaths.append(indexPath)
        let isExpanded = DataResponse[section].opened
        DataResponse[section].opened = !isExpanded
        if isExpanded {
            searchTable.deleteRows(at: indexPaths, with: .fade)
        } else {
            searchTable.insertRows(at: indexPaths, with: .fade)
        }
    }
    
    // MAKE: action uơdate status phrase
    @objc func onFavoriteFhrase(section:UIButton){
      var status:Int = 0
      let idPhrase = String(DataResponse[section.tag].id!)
      if DataResponse[section.tag].status! == 0 {
          DataResponse[section.tag].status! = 1
          status = 1
          self.showToast(message: "Đã thêm vào mục yêu thích")
      }else{
          DataResponse[section.tag].status! = 0
          status = 0
          self.showToast(message: "Đã gỡ khỏi mục yêu thích")
      }
      guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
      let managedContext = appDelegate.persistentContainer.viewContext
      let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "PhraseCR")
      fetchRequest.predicate = NSPredicate(format: "id = %@", idPhrase)
      do{
        let test = try managedContext.fetch(fetchRequest)
        let objectUpdate = test[0] as! NSManagedObject
        objectUpdate.setValue(status, forKey: "status")
         do{
            try managedContext.save()
         }
         catch
         {print(error)}
       }
      catch
      {print(error)}
    }
}

@available(iOS 13.0, *)
extension SearchViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            getAllData()
            searching = false
            searchTable.reloadData()
        }else{
            getDataSearch(string: searchText)
            searching = true
            searchTable.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = ""
        searchTable.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }

}
