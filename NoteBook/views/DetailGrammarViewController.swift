//
//  DetailGrammarViewController.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/27/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit
import CoreData
import WebKit

@available(iOS 13.0, *)
class DetailGrammarViewController: UIViewController {
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .darkContent
//    }
    
    @IBOutlet weak var btnBackToPre: UIButton!
    var grammarCoreData: [NSManagedObject] = []
    @IBOutlet weak var titleNavigation: UIButton!
    var html: String = ""
    @IBOutlet weak var viewContent: UITextView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet var viewDetailGrammar: UIView!
    @IBOutlet weak var containerContent: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewContent.textContainerInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
        getDataGrammarFormCoreData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupColor()
    }
    
    func setupColor() {
        let background = self.getValueLocal(key: "color-background")
        headerView.backgroundColor = UIColor(hexString: background)!
        viewContent.backgroundColor = UIColor(hexString: "#FFFFFF")!
        containerContent.backgroundColor = UIColor(hexString: "#FFFFFF")!
        viewDetailGrammar.backgroundColor = UIColor(hexString: background)!
    }
    
    @IBAction func backToPre(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    ///get data category  form core data
    func getDataGrammarFormCoreData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
          return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Grammar_translateCR")
        do {
              grammarCoreData = try managedContext.fetch(fetchRequest)
              for item in grammarCoreData{
                if String(item.value(forKey: "grammar_id")as! Int16) == self.getValueLocal(key: "grammar_id") && item.value(forKey: "language_code") as! String == self.getValueLocal(key: "language_code"){
                    titleNavigation.setTitle(" " + " " + self.getValueLocal(key: "nameGrammar"), for: .normal)
                  html = item.value(forKey: "translate") as! String
                }
              }
              let htmlData = NSString(string: html).data(using: String.Encoding.utf8.rawValue)
              let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue] as [NSAttributedString.DocumentReadingOptionKey : Any]
              let attributedString = try! NSAttributedString(data: htmlData!,options: options, documentAttributes: nil)
              viewContent.attributedText = attributedString
          } catch let error as NSError { print("Could not fetch. \(error), \(error.userInfo)") }
    }
}
