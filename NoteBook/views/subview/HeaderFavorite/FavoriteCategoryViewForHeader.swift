//
//  FavoriteCategoryViewForHeader.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/29/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

class FavoriteCategoryViewForHeader: UIView {

    @IBOutlet weak var actionFavorite: UIButton!
    @IBOutlet weak var imageChevron: UIImageView!
    @IBOutlet weak var imageCategory: UIImageView!
    @IBOutlet weak var txtNameCategory: UILabel!
    @IBOutlet weak var borderBottom: UIView!
    var statusFavorite:Int16 = 0
    
    @IBAction func actionClick(_ sender: Any) {
        if statusFavorite == 0{
            statusFavorite = 1
            imageChevron.image = UIImage(named: "chevron")
        }else{
            statusFavorite = 0
            imageChevron.image = UIImage(named: "chevron-up")
        }
    }
}
