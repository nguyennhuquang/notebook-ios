//
//  PopUpFavorite.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 12/9/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit
import CoreData

class PopUpFavorite: UIView {
    
    static let instance = PopUpFavorite()
    var idPhrase:Int16?
    @IBOutlet var parentView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var btnYes: CustomButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("PopUpFavorite", owner: self, options: nil)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    private func commonInit() {
        contentView.layer.cornerRadius = 10
        contentView.layer.shadowColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        contentView.layer.shadowOpacity = 0.9
        contentView.layer.shadowOffset = .zero
        contentView.layer.shadowRadius = 10
        parentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        parentView.backgroundColor = UIColor(white: 1, alpha: 0.7)
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func showAlert(title: String, message: String) {
        self.txtName.text = title
        self.txtTitle.text = message
        contentView.alpha = 0
        UIView.animate(withDuration: 0.3, animations: {
            UIApplication.shared.keyWindowInConnectedScenes?.addSubview(self.parentView)
            self.contentView.alpha = 1.0
        })

    }
    
    @IBAction func onClickYes(_ sender: Any) {
        parentView.removeFromSuperview()
    }
    
    @IBAction func onClickNo(_ sender: Any) {
        parentView.removeFromSuperview()
    }
    
}

extension UIApplication {
    var keyWindowInConnectedScenes: UIWindow? {
        return windows.first(where: { $0.isKeyWindow })
    }
}

