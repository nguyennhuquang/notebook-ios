//
//  ChangeSizeFont.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 12/30/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

class ChangeSizeFont: UIView {
    
    static let demo = ChangeSizeFont()
    @IBOutlet var parentView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var txtSmall: UILabel!
    @IBOutlet weak var txtNormal: UILabel!
    @IBOutlet weak var txtLarge: UILabel!
    @IBOutlet weak var btnSmaill: CustomButton!
    @IBOutlet weak var btnLarge: CustomButton!
    @IBOutlet weak var btnNormal: CustomButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("ChangeSizeFont", owner: self, options: nil)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    private func commonInit() {
        txtSmall.font = UIFont(name: "Cabin-Regular", size: 14)
        txtNormal.font = UIFont(name: "Cabin-Regular", size: 18)
        txtLarge.font = UIFont(name: "Cabin-Regular", size: 24)
        contentView.layer.cornerRadius = 5
        contentView.addShadow(offset: CGSize(width: 0, height: 0), color: .black, radius: 5, opacity: 0.4)
        parentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        parentView.backgroundColor = UIColor(white: 1, alpha: 0.9)
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        parentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(noneHide)))

    }

    func showAlert() {
        contentView.alpha = 0
        UIView.animate(withDuration: 0.3, animations: {
            UIApplication.shared.keyWindowInConnectedScenes?.addSubview(self.parentView)
            self.contentView.alpha = 1.0
        })
    }
    
    
    @objc func noneHide(){
        self.parentView.removeFromSuperview()
    }
    
//    @IBAction func actionSmaill(_ sender: Any) {
//        self.setValueLocal(value: "14", key: "font-size")
//        btnNormal.setImage(UIImage(named: "selectFont"), for: .normal)
//        btnLarge.setImage(UIImage(named: "selectFont"), for: .normal)
//        btnSmaill.setImage(UIImage(named: "activeFont"), for: .normal)
//    }
//
//    @IBAction func actionNormal(_ sender: Any) {
//        self.setValueLocal(value: "18", key: "font-size")
//        btnNormal.setImage(UIImage(named: "activeFont"), for: .normal)
//        btnLarge.setImage(UIImage(named: "selectFont"), for: .normal)
//        btnSmaill.setImage(UIImage(named: "selectFont"), for: .normal)
//    }
//
//    @IBAction func actionLarge(_ sender: Any) {
//        self.setValueLocal(value: "24", key: "font-size")
//        btnNormal.setImage(UIImage(named: "selectFont"), for: .normal)
//        btnLarge.setImage(UIImage(named: "activeFont"), for: .normal)
//        btnSmaill.setImage(UIImage(named: "selectFont"), for: .normal)
//    }
    
}

