//
//  viewForHeader.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/23/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

class viewForHeader: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var actionView: UIButton!
    @IBOutlet weak var txtNamePhrase: UILabel!
    @IBOutlet weak var actionFavorite: UIButton!
    
    var statusFavorite:Int16 = 0
    
    @IBAction func favorite(_ sender: Any) {
        if statusFavorite == 0{
            statusFavorite = 1
            actionFavorite.setImage(UIImage(named: "favorite-select"), for: .normal)
        }else{
            statusFavorite = 0
            actionFavorite.setImage(UIImage(named: "favorite"), for: .normal)
        }
    }

    
}
