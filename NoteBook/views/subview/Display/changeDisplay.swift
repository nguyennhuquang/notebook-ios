//
//  changeDisplay.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 12/27/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

class changeDisplay: UIView {

    static let demo = changeDisplay()
    
    @IBOutlet var parentView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var txtDark: UILabel!
    @IBOutlet weak var txtlight: UILabel!
    @IBOutlet weak var viewLight: UIView!
    @IBOutlet weak var viewDark: UIView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("changeDisplay", owner: self, options: nil)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }

    private func commonInit() {
        contentView.layer.cornerRadius = 5
        contentView.addShadow(offset: CGSize(width: 0, height: 0), color: .black, radius: 5, opacity: 0.4)
        parentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        parentView.backgroundColor = UIColor(white: 1, alpha: 0.9)
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        viewLight.backgroundColor = UIColor(hexString: "#EFEFEF")!
        viewLight.layer.cornerRadius = 5
        viewLight.layer.borderColor = #colorLiteral(red: 0.160970211, green: 0.7804604769, blue: 0.6089003682, alpha: 1)
        viewLight.layer.borderWidth = 2
        txtlight.textColor = #colorLiteral(red: 0.160970211, green: 0.7804604769, blue: 0.6089003682, alpha: 1)
        parentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(noneHide)))
        
        viewDark.backgroundColor = UIColor(hexString: "#C4C4C4")!
        viewDark.layer.cornerRadius = 5
        txtDark.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        viewDark.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        viewDark.layer.borderWidth = 0
    }

    func showAlert() {
        contentView.alpha = 0
        UIView.animate(withDuration: 0.3, animations: {
            UIApplication.shared.keyWindowInConnectedScenes?.addSubview(self.parentView)
            self.contentView.alpha = 1.0
        })
    }
    
    
    @objc func noneHide(){
        self.parentView.removeFromSuperview()
    }

}




