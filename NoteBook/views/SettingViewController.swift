//
//  SettingViewController.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 12/13/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {
//    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .darkContent
//    }
//    

    @IBOutlet var containerView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var btnBackToPre: UIButton!
    @IBOutlet weak var borderBottomHeader: UIView!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var contentCNC: UIView!
    @IBOutlet weak var containerCC: UIView!
    @IBOutlet weak var containerGD: UIView!
    @IBOutlet weak var txtValueCC: UILabel!
    @IBOutlet weak var txtValueGD: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        let background = self.getValueLocal(key: "color-background")
        if background == "#F6F6F6" {
            txtValueGD.text = "Light"
            changeDisplay.demo.viewLight.layer.borderColor = #colorLiteral(red: 0.160970211, green: 0.7804604769, blue: 0.6089003682, alpha: 1)
            changeDisplay.demo.viewLight.layer.borderWidth = 2
            changeDisplay.demo.txtlight.textColor = #colorLiteral(red: 0.160970211, green: 0.7804604769, blue: 0.6089003682, alpha: 1)
            changeDisplay.demo.txtDark.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            changeDisplay.demo.viewDark.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            changeDisplay.demo.viewDark.layer.borderWidth = 0
        }else {
            txtValueGD.text = "Dark"
            changeDisplay.demo.viewDark.layer.borderColor = #colorLiteral(red: 0.160970211, green: 0.7804604769, blue: 0.6089003682, alpha: 1)
            changeDisplay.demo.viewDark.layer.borderWidth = 2
            changeDisplay.demo.txtDark.textColor = #colorLiteral(red: 0.160970211, green: 0.7804604769, blue: 0.6089003682, alpha: 1)
            changeDisplay.demo.txtlight.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            changeDisplay.demo.viewLight.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            changeDisplay.demo.viewLight.layer.borderWidth = 0
        }
        let fontSize = self.getValueLocal(key: "font-size")
        if fontSize == "14" {
            txtValueCC.text = "Small"
        }else if fontSize == "18" {
            txtValueCC.text = "Normal"
        }else {
            txtValueCC.text = "Large"
        }
        navigationController?.navigationBar.barStyle = .black
        changeDisplay.demo.viewLight.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectLight)))
        changeDisplay.demo.viewDark.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectDark)))
        
        ChangeSizeFont.demo.btnSmaill.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectSmail)))
        ChangeSizeFont.demo.btnNormal.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectNormal)))
        ChangeSizeFont.demo.btnLarge.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectLarge)))
        
        setupView()
    }
    
    @objc func selectLight(){
        self.setValueLocal(value: "#F6F6F6", key: "color-background")
        changeDisplay.demo.parentView.removeFromSuperview()
        changeDisplay.demo.viewLight.layer.borderColor = #colorLiteral(red: 0.160970211, green: 0.7804604769, blue: 0.6089003682, alpha: 1)
        changeDisplay.demo.viewLight.layer.borderWidth = 2
        changeDisplay.demo.txtlight.textColor = #colorLiteral(red: 0.160970211, green: 0.7804604769, blue: 0.6089003682, alpha: 1)
        changeDisplay.demo.txtDark.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        changeDisplay.demo.viewDark.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        changeDisplay.demo.viewDark.layer.borderWidth = 0
        changeDisplay.demo.parentView.removeFromSuperview()
        self.navigationController?.popViewController(animated: true)
        txtValueGD.text = "Light"
    }
    
    @objc func selectDark(){
        self.setValueLocal(value: "#C4C4C4", key: "color-background")
        changeDisplay.demo.viewDark.layer.borderColor = #colorLiteral(red: 0.160970211, green: 0.7804604769, blue: 0.6089003682, alpha: 1)
        changeDisplay.demo.viewDark.layer.borderWidth = 2
        changeDisplay.demo.txtDark.textColor = #colorLiteral(red: 0.160970211, green: 0.7804604769, blue: 0.6089003682, alpha: 1)
        changeDisplay.demo.txtlight.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        changeDisplay.demo.viewLight.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        changeDisplay.demo.viewLight.layer.borderWidth = 0
        changeDisplay.demo.parentView.removeFromSuperview()
        txtValueGD.text = "Dark"
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func selectSmail(){
        self.setValueLocal(value: "14", key: "font-size")
        ChangeSizeFont.demo.btnNormal.setImage(UIImage(named: "selectFont"), for: .normal)
        ChangeSizeFont.demo.btnLarge.setImage(UIImage(named: "selectFont"), for: .normal)
        ChangeSizeFont.demo.btnSmaill.setImage(UIImage(named: "activeFont"), for: .normal)
        ChangeSizeFont.demo.parentView.removeFromSuperview()
        txtValueCC.text = "Small"
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func selectNormal(){
        self.setValueLocal(value: "18", key: "font-size")
        ChangeSizeFont.demo.btnNormal.setImage(UIImage(named: "activeFont"), for: .normal)
        ChangeSizeFont.demo.btnLarge.setImage(UIImage(named: "selectFont"), for: .normal)
        ChangeSizeFont.demo.btnSmaill.setImage(UIImage(named: "selectFont"), for: .normal)
        ChangeSizeFont.demo.parentView.removeFromSuperview()
        txtValueCC.text = "Normal"
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func selectLarge(){
        self.setValueLocal(value: "24", key: "font-size")
        ChangeSizeFont.demo.btnNormal.setImage(UIImage(named: "selectFont"), for: .normal)
        ChangeSizeFont.demo.btnLarge.setImage(UIImage(named: "activeFont"), for: .normal)
        ChangeSizeFont.demo.btnSmaill.setImage(UIImage(named: "selectFont"), for: .normal)
        ChangeSizeFont.demo.parentView.removeFromSuperview()
        txtValueCC.text = "Large"
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backToPre(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func setupView() {
        let background = self.getValueLocal(key: "color-background")
        containerView.backgroundColor = UIColor(hexString: background)!
        headerView.backgroundColor = UIColor(hexString: background)!
        borderBottomHeader.backgroundColor =  UIColor(hexString: "#DFDFDF")!
        txtTitle.textColor = UIColor(hexString: "#7F7F7F")!
        contentCNC.backgroundColor =  UIColor(hexString: "#FFFFFF")!
        containerCC.backgroundColor =  UIColor(hexString: "#FFFFFF")!
        containerGD.backgroundColor =  UIColor(hexString: "#FFFFFF")!
        containerGD.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showPopUpChangeDisplay)))
        containerCC.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showPopUpChangeFont)))
    }

    @objc func showPopUpChangeDisplay(){
        changeDisplay.demo.showAlert()
    }
    
    @objc func showPopUpChangeFont(){
        ChangeSizeFont.demo.showAlert()
    }
    
}
