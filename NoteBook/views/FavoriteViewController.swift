//
//  FavoriteViewController.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/28/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit
import CoreData

@available(iOS 13.0, *)
@available(iOS 13.0, *)
class FavoriteViewController: UIViewController {

//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .darkContent
//    }
    
    var responseCategoryCoreData: [NSManagedObject] = []
    var responseFhraseCoreData: [NSManagedObject] = []
    var responseTranslateCoreData: [NSManagedObject] = []
    var FavoriteAray:[Favorite] = []
    var favoriteDataTableView:[Favorite] = []
    var PhraseArray:[PhraseFavoriteData] = []
    var DataIdCategotyFavorite:[Int16] = []
    
    
    
    @IBOutlet weak var txtNotAvailableData: UILabel!
    @IBOutlet weak var containerViewHeader: UIView!
    @IBOutlet weak var titleNavigation: UILabel!
    @IBOutlet weak var favoriteTableView: UITableView!
    @IBOutlet var viewFavorite: UIView!
    @IBOutlet weak var containerTableView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        favoriteTableView.dataSource = self
        favoriteTableView.delegate = self
        favoriteTableView.register(UINib(nibName: "CategoryFavoriteTableViewCell", bundle: nil), forCellReuseIdentifier: "CategoryFavoriteTableViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let bottomBorder: CALayer = CALayer()
        bottomBorder.frame = CGRect(x: 0.0, y: containerViewHeader.frame.size.height-1, width: containerViewHeader.frame.width, height: 1.0)
        bottomBorder.backgroundColor = #colorLiteral(red: 0.8043097854, green: 0.9305560589, blue: 0.8938990235, alpha: 1)
        containerViewHeader.layer.addSublayer(bottomBorder)
        favoriteTableView.separatorStyle = .none
        txtNotAvailableData.setLineHeight(lineHeight: 1.2)
        setupColor()
        getCategoryFormCoreData()
        getFavoriteFormCoreData()
    }
    
    func setupColor() {
        let background = self.getValueLocal(key: "color-background")
        viewFavorite.backgroundColor = UIColor(hexString: background)!
        favoriteTableView.backgroundColor = UIColor(hexString: "#FFFFFF")!
        containerTableView.backgroundColor = UIColor(hexString: "#FFFFFF")!
        containerViewHeader.backgroundColor = UIColor(hexString: background)!
    }
    
    func getFavoriteFormCoreData() {
        /// push từ yêu thích vào category
        for itemPhrase in PhraseArray{
          for itemFavorite in FavoriteAray{
            if itemFavorite.id == itemPhrase.category_id{
               itemFavorite.phrases.append(itemPhrase)
            }
          }
        }
        /// xử lí các phần tử trufng nhau trong mảng
        favoriteDataTableView = []
        for item in FavoriteAray{
            if item.phrases.count > 0 {
                favoriteDataTableView.append(item)
                favoriteTableView.reloadData()
            }
        }
        titleNavigation.text = "Mục yêu thích (" + String(favoriteDataTableView.count) + ")"
        favoriteTableView.isHidden = favoriteDataTableView.count == 0 ? true : false
    }
    
    ///Get data from category by id
    func getPhraseFormCoreData(id:String){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
          return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        do {
          let fetchRequestPhrase = NSFetchRequest<NSManagedObject>(entityName: "PhraseCR")
          fetchRequestPhrase.predicate = NSPredicate(format: "category_id = %@", id)
          responseFhraseCoreData = try managedContext.fetch(fetchRequestPhrase)
          for itemPhrase in responseFhraseCoreData {
              do {
                  let fetchRequestTranslate = NSFetchRequest<NSManagedObject>(entityName: "TranslateCR")
                  responseTranslateCoreData = try managedContext.fetch(fetchRequestTranslate)
                  for itemTranslate in responseTranslateCoreData {
                    if  itemPhrase.value(forKey: "id") as! Int16 == itemTranslate.value(forKey: "phrase_id") as! Int16 && itemPhrase.value(forKey: "status") as! Int16 == 1 && itemTranslate.value(forKey: "language_code") as! String == self.getValueLocal(key: "language_code"){
                          PhraseArray.append(PhraseFavoriteData(
                              opened: false,
                              name: itemPhrase.value(forKey: "name") as! String,
                              status: itemPhrase.value(forKey: "status") as! Int16,
                              translate: itemTranslate.value(forKey: "translate") as! String,
                              sound: itemTranslate.value(forKey: "sound") as! Data,
                              id: itemPhrase.value(forKey: "id") as! Int16,
                              category_id: itemPhrase.value(forKey: "category_id") as! Int16)
                          )
                      }
                  }
              } catch let error as NSError {print("Could not fetch. \(error), \(error.userInfo)")}
          }
         } catch let error as NSError {print("Could not fetch. \(error), \(error.userInfo)")}
    }

    ///Get the categories with favorite words
    func getCategoryFormCoreData() {
        PhraseArray = []
        FavoriteAray = []
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
          return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CategoryCR")
        do {
          responseCategoryCoreData = try managedContext.fetch(fetchRequest)
          for itemCategory in responseCategoryCoreData{
              FavoriteAray.append(Favorite(
                id:  itemCategory.value(forKey: "id") as! Int16,
                name: itemCategory.value(forKey: "name") as! String,
                image: itemCategory.value(forKey: "image") as! Data,
                isExpanded: false,
                classify: "phrase",
                phrases: [])
              )
          }
          for item in FavoriteAray{
            getPhraseFormCoreData(id: String(item.id!))
          }
        } catch let error as NSError {
          print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}


@available(iOS 13.0, *)
@available(iOS 13.0, *)
extension FavoriteViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !favoriteDataTableView[section].isExpanded {
            return 0
        }
        return favoriteDataTableView[section].phrases.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = favoriteTableView.dequeueReusableCell(withIdentifier: "CategoryFavoriteTableViewCell") as! CategoryFavoriteTableViewCell
        cell.urlSound = favoriteDataTableView[indexPath.section].phrases[indexPath.row].sound!
        cell.txtPhraseName.text = favoriteDataTableView[indexPath.section].phrases[indexPath.row].name!
        let str : String = self.getValueLocal(key: "font-size")
        let secStr : NSString = str as NSString
        let fontSize : CGFloat = CGFloat(secStr.doubleValue)
        cell.txtPhraseName.font = UIFont(name: "Cabin-Medium", size: fontSize)
        cell.txtTranslate.text = favoriteDataTableView[indexPath.section].phrases[indexPath.row].translate!
        if favoriteDataTableView[indexPath.section].phrases[indexPath.row].opened == false {
            cell.closeCell()
        }else{
            cell.openCell()
        }
        cell.actionSHowHideView.addTarget(self, action: #selector(showHideTranslate), for: .touchUpInside)
        cell.btnFavorite.addTarget(self, action: #selector(showPopUpRemoveFavorite), for: .touchUpInside)
        cell.actionSHowHideView.tag = (indexPath.section*100)+indexPath.row
        cell.btnFavorite.tag = (indexPath.section*100)+indexPath.row
        cell.borderFoot.isHidden = true
        cell.borderHeader.isHidden = false
        return cell
    }
    
    @objc func showHideTranslate(button: UIButton) {
        let section = button.tag / 100
        let row = button.tag % 100
        favoriteDataTableView[section].phrases[row].opened = !favoriteDataTableView[section].phrases[row].opened
        favoriteTableView.beginUpdates()
        favoriteTableView.setNeedsDisplay()
        favoriteTableView.endUpdates()
    }
    
    @objc func showPopUpRemoveFavorite(button: UIButton) {
        let section = button.tag / 100
        let row = button.tag % 100
        PopUpFavorite.instance.showAlert(title: "Xóa khỏi Mục Ưa Thích", message: "Bạn có chắc muốn xóa mục đã lựa chọn?")
        PopUpFavorite.instance.btnYes.addTarget(self, action: #selector(updateStatusPhrase), for: .touchUpInside)
        PopUpFavorite.instance.btnYes.tag = (section*100)+row
    }
    
    
    @objc func updateStatusPhrase(button: UIButton) {
        let section = button.tag / 100
        let row = button.tag % 100
        let indexPath = IndexPath(row: row, section: section)
        let id = String(favoriteDataTableView[section].phrases[row].id!)
        favoriteDataTableView[section].phrases.remove(at: row)
        favoriteTableView.beginUpdates()
        favoriteTableView.deleteRows(at: [indexPath], with: .fade)
        favoriteTableView.endUpdates()
        updatCoreData(id: id)
        if favoriteDataTableView[section].phrases.count == 0 {
            favoriteDataTableView.remove(at: section)
            favoriteTableView.deleteSections(NSIndexSet(index: section) as IndexSet, with: .fade)
            titleNavigation.text = "Mục yêu thích (" + String(favoriteDataTableView.count) + ")"
        }else{
            
        }
        favoriteTableView.reloadData()
        favoriteTableView.isHidden = favoriteDataTableView.count == 0 ? true : false
    }
    
    func updatCoreData(id:String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "PhraseCR")
        fetchRequest.predicate = NSPredicate(format: "id = %@", id)
        do{
          let test = try managedContext.fetch(fetchRequest)
          let objectUpdate = test[0] as! NSManagedObject
          objectUpdate.setValue(0, forKey: "status")
          do{ try managedContext.save() }
          catch { print(error)} }
        catch { print(error) }
    }
    
}
@available(iOS 13.0, *)
extension FavoriteViewController: UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return favoriteDataTableView.count
    }
    
    /// add viewHeader to table view
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = Bundle.main.loadNibNamed("FavoriteCategoryViewForHeader", owner: self, options: nil)?.first as! FavoriteCategoryViewForHeader
        headerCell.txtNameCategory.text = favoriteDataTableView[section].name!
        let str : String = self.getValueLocal(key: "font-size")
        let secStr : NSString = str as NSString
        let fontSize : CGFloat = CGFloat(secStr.doubleValue)
        headerCell.txtNameCategory.font = UIFont(name: "Cabin-Medium", size: fontSize)
        headerCell.borderBottom.isHidden = true
        headerCell.imageCategory.image = UIImage(data: favoriteDataTableView[section].image!)
    
        headerCell.actionFavorite.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
//        headerCell.actionClick(favoriteDataTableView[section].isExpanded)
        headerCell.actionFavorite.tag = section
        return headerCell
    }

    /// set height viewHeader
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 62
    }

    ///add view footer to table view
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = #colorLiteral(red: 0.8317679763, green: 0.9580102563, blue: 0.9213493466, alpha: 1)
        return footerView
    }

    ///set height view footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    @objc func handleExpandClose(button: UIButton) {
        let section = button.tag
        var indexPaths = [IndexPath]()
        for row in favoriteDataTableView[section].phrases.indices {
            let indexPath = IndexPath(row: row, section: section)
            indexPaths.append(indexPath)
        }
        let isExpanded = favoriteDataTableView[section].isExpanded
        favoriteDataTableView[section].isExpanded = !isExpanded
        if isExpanded {
            for indexRow in favoriteDataTableView[section].phrases.indices {
                favoriteDataTableView[section].phrases[indexRow].opened = false
            }
            favoriteTableView.deleteRows(at: indexPaths, with: .fade)
        } else {
            favoriteTableView.insertRows(at: indexPaths, with: .fade)
            let demo = NSIndexPath(row: favoriteDataTableView[section].phrases.count - 1, section: section)
            favoriteTableView.scrollToRow(at: demo as IndexPath, at: .none, animated: true)
        }
    }
}

extension UIScrollView {
    func scrollsToBottom(animated: Bool) {
        let bottomOffset = CGPoint(x: contentOffset.x,
                                   y: contentSize.height - bounds.height + adjustedContentInset.bottom)
        setContentOffset(bottomOffset, animated: animated)
    }
}

