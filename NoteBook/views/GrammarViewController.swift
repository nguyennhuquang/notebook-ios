//
//  GrammarViewController.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/26/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit
import CoreData

@available(iOS 13.0, *)
class GrammarViewController: UIViewController {
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .darkContent
//    }
    
    var actionStudyTips:Int = 0
    var actionGrammar:Int = 0
    var grammarCoreData: [NSManagedObject] = []
    @IBOutlet weak var txtTitleGrammar: UILabel!
    @IBOutlet weak var btnGrammar: UIButton!
    @IBOutlet weak var btbStudyTips: UIButton!
    @IBOutlet weak var tabBarContainer: UIView!
    @IBOutlet weak var borderBottomSelected: UIView!
    @IBOutlet weak var grammarTableView: UITableView!
    @IBOutlet weak var headerContainer: UIView!
    @IBOutlet var viewGrammar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        grammarTableView.dataSource = self
        grammarTableView.delegate = self
        grammarTableView.register(UINib(nibName: "GrammarTableViewCell", bundle: nil), forCellReuseIdentifier: "GrammarTableViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let topBorder: CALayer = CALayer()
        topBorder.frame = CGRect(x: 0.0, y: headerContainer.frame.size.height-1, width: headerContainer.frame.width, height: 1.0)
        topBorder.backgroundColor = #colorLiteral(red: 0.8043097854, green: 0.9305560589, blue: 0.8938990235, alpha: 1)
        headerContainer.layer.addSublayer(topBorder)
        grammarTableView.separatorStyle = .none
        setupColor()
        getDataGrammarFormCoreData(classify_name: "grammar", statusAnimation: false)
        
        ///set active tabbar item
        btnGrammar.setTitleColor(#colorLiteral(red: 0.160970211, green: 0.7804604769, blue: 0.6089003682, alpha: 1), for:.normal)
        btbStudyTips.setTitleColor(#colorLiteral(red: 0.6979619861, green: 0.698083818, blue: 0.6979543567, alpha: 1), for: .normal)
        actionStudyTips = 0
        actionGrammar = 1
        let yPosition = borderBottomSelected.frame.origin.y
        let width = borderBottomSelected.frame.size.width
        let height = borderBottomSelected.frame.size.height
        borderBottomSelected.frame = CGRect(x: 25, y: yPosition, width: width, height: height)
    }
    
    func setupColor() {
        let background = self.getValueLocal(key: "color-background")
        tabBarContainer.backgroundColor = UIColor(hexString: "#FFFFFF")!
        headerContainer.backgroundColor = UIColor(hexString: background)!
        grammarTableView.backgroundColor = UIColor(hexString: "#FFFFFF")!
        viewGrammar.backgroundColor = UIColor(hexString: background)!
    }
    
    @IBAction func onSelectedGrammar(_ sender: Any) {
        if actionGrammar == 0 {
            actionStudyTips = 0
            actionGrammar = 1
            let xPosition = borderBottomSelected.frame.origin.x - 95
            let yPosition = borderBottomSelected.frame.origin.y
            let width = borderBottomSelected.frame.size.width
            let height = borderBottomSelected.frame.size.height
            UIView.animate(withDuration: 0.2, animations: {
                self.borderBottomSelected.frame = CGRect(x: xPosition, y: yPosition, width: width, height: height)
            })
            btnGrammar.setTitleColor(#colorLiteral(red: 0.160970211, green: 0.7804604769, blue: 0.6089003682, alpha: 1), for:.normal)
            btbStudyTips.setTitleColor(#colorLiteral(red: 0.6979619861, green: 0.698083818, blue: 0.6979543567, alpha: 1), for: .normal)
            getDataGrammarFormCoreData(classify_name: "grammar", statusAnimation: true)
        }
    }
    
    @IBAction func onSelectedStudyTips(_ sender: Any) {
        if actionStudyTips == 0 {
            actionStudyTips = 1
            actionGrammar = 0
            let xPosition = borderBottomSelected.frame.origin.x + 95
            let yPosition = borderBottomSelected.frame.origin.y
            let width = borderBottomSelected.frame.size.width
            let height = borderBottomSelected.frame.size.height
            UIView.animate(withDuration: 0.2, animations: {
                self.borderBottomSelected.frame = CGRect(x: xPosition, y: yPosition, width: width, height: height)
            })
            btbStudyTips.setTitleColor(#colorLiteral(red: 0.160970211, green: 0.7804604769, blue: 0.6089003682, alpha: 1), for:.normal)
            btnGrammar.setTitleColor(#colorLiteral(red: 0.6979619861, green: 0.698083818, blue: 0.6979543567, alpha: 1), for: .normal)
            getDataGrammarFormCoreData(classify_name: "study_tips", statusAnimation: true)
        }
    }
    
    ///get data category  form core data
    func getDataGrammarFormCoreData(classify_name: String, statusAnimation:Bool) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
          return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "GrammarCR")
        fetchRequest.predicate = NSPredicate(format: "classify_name = %@", classify_name)
        do {
          grammarCoreData = try managedContext.fetch(fetchRequest)
            if statusAnimation {
                let transition = CATransition()
                transition.type = CATransitionType.push
                transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                transition.fillMode = CAMediaTimingFillMode.forwards
                if classify_name == "grammar" && statusAnimation{
                  transition.subtype = CATransitionSubtype.fromRight
                }else{
                  transition.subtype = CATransitionSubtype.fromLeft
                }
                transition.duration = 0.3
                self.grammarTableView.layer.add(transition, forKey: "UITableViewReloadDataAnimationKey")
            }
          self.grammarTableView.reloadData()
        } catch let error as NSError {
          print("Could not fetch. \(error), \(error.userInfo)")
        }
    }

}

@available(iOS 13.0, *)
extension GrammarViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return grammarCoreData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = grammarTableView.dequeueReusableCell(withIdentifier: "GrammarTableViewCell") as! GrammarTableViewCell
        cell.txtNameGrammar.text = (grammarCoreData[indexPath.row].value(forKey: "name") as! String)
        let str : String = self.getValueLocal(key: "font-size")
        let secStr : NSString = str as NSString
        let fontSize : CGFloat = CGFloat(secStr.doubleValue)
        cell.txtNameGrammar.font = UIFont(name: "Cabin-Medium", size: fontSize)
        if grammarCoreData[indexPath.row].value(forKey: "status") as! Int16 == 1{
            cell.actionFavorite.setImage(UIImage(named: "favorite-select"), for: .normal)
        }else{
            cell.actionFavorite.setImage(UIImage(named: "favorite"), for: .normal)
        }
        cell.actionFavorite.tag = Int(grammarCoreData[indexPath.row].value(forKey: "id") as! Int16)
        cell.statusFavorite = grammarCoreData[indexPath.row].value(forKey: "status") as! Int16
        return cell
    }
}

@available(iOS 13.0, *)
extension GrammarViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.NavigationView(Identifier: "DetailGrammarViewController", animated: true)
        self.setValueLocal(value: String(grammarCoreData[indexPath.row].value(forKey: "id") as! Int16), key: "grammar_id")
        self.setValueLocal(value: grammarCoreData[indexPath.row].value(forKey: "name") as! String, key: "nameGrammar")
    }
}
