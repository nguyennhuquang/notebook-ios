//
//  LanguageViewController.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/18/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit

class LanguageViewController: UIViewController {
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .darkContent
//    }
    
    @IBOutlet weak var languageTable: UITableView!
    @IBOutlet var viewLanguage: UIView!
    @IBOutlet weak var titleLanguage: UILabel!
    
    var Identifier:String = "LanguageTableCell"
    var Languages:[Language] = [
        Language(name: "Tiếng Anh", icon: "en", image: "tienganh", code: "en"),
        Language(name: "Tiếng Thái Lan", icon: "th", image: "tiengthai", code: "th"),
        Language(name: "Tiếng Nhật Bản", icon: "ja", image: "tiengnhat", code: "ja"),
        Language(name: "Tiếng Hàn Quốc", icon: "ko", image: "tienghan", code: "ko"),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        languageTable.register(UINib(nibName: "LanguageTableView", bundle: nil), forCellReuseIdentifier: Identifier)
        languageTable.dataSource = self
        languageTable.delegate = self
        languageTable.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupColor()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setupColor() {
        viewLanguage.backgroundColor = UIColor(hexString: "#E5E5E5")!
        titleLanguage.textColor = UIColor(hexString: "#000000")!
    }
}


extension LanguageViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Languages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let languageCell = languageTable.dequeueReusableCell(withIdentifier: Identifier) as! LanguageTableView
        languageCell.layer.cornerRadius = 8
        languageCell.backgroundLanguage.layer.cornerRadius = 8
        languageCell.setBackgroundLanguage(nameImage: Languages[indexPath.row].image!)
        languageCell.setIconLanguage(nameIcon: Languages[indexPath.row].icon!)
        languageCell.setNameLanguage(name: Languages[indexPath.row].name!)
        if self.getValueLocal(key: "language_code") == Languages[indexPath.row].code {
            languageCell.activeLanguage.isHidden = false
        }else{
            languageCell.activeLanguage.isHidden = true
        }
        return languageCell
    }
}

extension LanguageViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.setValueLocal(value: Languages[indexPath.row].code!, key: "language_code")
        languageTable.reloadData()
        self.NavigationView(Identifier: "Home", animated: true)
    }
}

