//
//  HomeViewController.swift
//  NoteBook
//
//  Created by Nguyễn Quang on 11/18/19.
//  Copyright © 2019 NGUYENQUANG_VN. All rights reserved.
//

import UIKit
import CoreData

@available(iOS 13.0, *)
class HomeViewController: UIViewController {
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .darkContent
//    }
    
    var CategoyResponse: Array<DataCategory> = Array()
    var PhraseResponse: Array<Phrase> = Array()
    var TranslateResponse: Array<Translate> = Array()
    
    var DataGrammarResponse: Array<Grammars> = Array()
    var DataGrammarTranslate: Array<GramarTranslate> = Array()
    
    var categoryCoreData: [NSManagedObject] = []
    var insertCoreData: [NSManagedObject] = []
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var CollectionCategory: UICollectionView!
    @IBOutlet var viewHome: UIView!
    
    let Identifier:String = "HomeCollectionViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CollectionCategory.dataSource = self
        CollectionCategory.delegate = self
        CollectionCategory.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: Identifier)
        CollectionCategory?.contentInset = UIEdgeInsets(top: 0, left: 24, bottom:62 , right: 24)
        if let layout = CollectionCategory?.collectionViewLayout as? PinterestLayout {
          layout.delegate = self
        }
        getDataCategoryFormCoreData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        setupView()
        navigationController?.navigationBar.barStyle = .black
        CollectionCategory.reloadData()
        checkFirstTimeOnTheApp()
    }
    
    func setupView() {
        let background = self.getValueLocal(key: "color-background")
        viewHome.backgroundColor = UIColor(hexString: background)!
        headerView.backgroundColor = UIColor(hexString: background)!
        CollectionCategory.backgroundColor =  UIColor(hexString: "#FFFFFF")!
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
    }
    
    ///get data category  form core data
    func getDataCategoryFormCoreData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
          return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CategoryCR")
        
        do {
          categoryCoreData = try managedContext.fetch(fetchRequest)
          self.CollectionCategory.reloadData()
        } catch let error as NSError {
          print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    ///The function checks first time into the app
    func checkFirstTimeOnTheApp() {
        if(self.getValueLocal(key: "language_code") == "nil"){
            self.NavigationView(Identifier: "Language", animated: false)
            getAllCategory()
            getAllGrammar()
            self.setValueLocal(value: "18", key: "font-size")
            self.setValueLocal(value: "#F6F6F6", key: "color-background")
        }
    }
    
    func getAllCategory()  {
        ApiService.getAllCategory(dataCallback: { (data) in
            let caregoryResponse = Category.init(dictionary: data as! NSDictionary)
            self.CategoyResponse = caregoryResponse!.data!
            for item in self.CategoyResponse{
                ///insert Category
                self.insertCategoryFormCoreData(name: item.name!, image: item.image!, id: Int16(item.id!))
                self.PhraseResponse = item.phrase!
                
                for itemPhrase in self.PhraseResponse{
                    ///insert Phrase
                    self.insertPhraseFormCoreData(category_id: Int16(itemPhrase.category_id!), id: Int16(itemPhrase.id!), name: itemPhrase.name!, status: 0)
                    self.TranslateResponse = itemPhrase.translate!
                    
                    for itemTranslate in self.TranslateResponse{
                        ///insert Translate
                        self.insertTranslateFormCoreData(language_code: itemTranslate.language_code!, sound: itemTranslate.sound!, translate: itemTranslate.translate!, id: Int16(itemTranslate.id!), phrase_id: Int16(itemTranslate.phrase_id!))
                    }
                }
            }
            
        }) { (error) in
            print(error)
        }
    }
    
    func getAllGrammar() {
        ApiService.getAllGrammar(dataCallback: { (data) in
            let grammarResponse = Grammar.init(dictionary: data as! NSDictionary)
            self.DataGrammarResponse = grammarResponse!.data!
            for item in self.DataGrammarResponse{
                self.insertGrammarFormCoreData(id: Int16(item.id!), name: item.name!, classify_name: item.classify_name!, classify_id: Int16(item.classify_id!), status: 0)
                self.DataGrammarTranslate = item.translate!
                for itemTranslate in self.DataGrammarTranslate{
                    self.insertGrammarTranslateFormCoreData(id: Int16(itemTranslate.id!), grammar_id: Int16(itemTranslate.grammar_id!), language_code: itemTranslate.language_code!, translate: itemTranslate.translate!)
                }
            }
        }){ (error) in
            print(error)
        }
    }
    
    ///The function go to the LanguageVỉewController
    @IBAction func onChangeLanguage(_ sender: Any) {
        self.NavigationView(Identifier: "Language", animated: true)
    }
    
    @IBAction func onSearchApp(_ sender: Any) {
        self.NavigationView(Identifier: "SearchViewController", animated: true)
    }
    
    @IBAction func onSetting(_ sender: Any) {
        self.NavigationView(Identifier: "SettingViewController", animated: true)
    }
    
    /// insert data to Category Form CoreData
    func insertCategoryFormCoreData(name:String, image:String, id:Int16) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
          return
        }

        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "CategoryCR", in: managedContext)!
        let categoryItem = NSManagedObject(entity: entity, insertInto: managedContext)
        categoryItem.setValue(name, forKeyPath: "name")
        let url = URL(string: image)
         do {
             let data = try Data(contentsOf: url!)
             categoryItem.setValue(data, forKey: "image")
         } catch  {
             print("NOT INTERNET")
         }
        categoryItem.setValue(id, forKeyPath: "id")

        do {
           try managedContext.save()
            self.insertCoreData.append(categoryItem)
           print("insert data CategoryCR success")
        } catch let error as NSError {
           print("Could not save. \(error), \(error.userInfo)")
        }

    }
    
    /// insert data to Phrase Form CoreData
    func insertPhraseFormCoreData(category_id:Int16, id:Int16, name:String, status:Int16) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
          return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "PhraseCR", in: managedContext)!
        let categoryItem = NSManagedObject(entity: entity, insertInto: managedContext)
        categoryItem.setValue(category_id, forKeyPath: "category_id")
        categoryItem.setValue(id, forKeyPath: "id")
        categoryItem.setValue(name, forKeyPath: "name")
        categoryItem.setValue(status, forKeyPath: "status")
        do {
           try managedContext.save()
           self.insertCoreData.append(categoryItem)
           print("insert data Phrases success")
        } catch let error as NSError {
           print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    /// insert data to Translate Form CoreData
    func insertTranslateFormCoreData(language_code:String, sound:String, translate:String, id:Int16, phrase_id:Int16) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
          return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "TranslateCR", in: managedContext)!
        let categoryItem = NSManagedObject(entity: entity, insertInto: managedContext)
        categoryItem.setValue(id, forKeyPath: "id")
        categoryItem.setValue(phrase_id, forKeyPath: "phrase_id")
        categoryItem.setValue(language_code, forKeyPath: "language_code")
        let url = URL(string: sound)
         do {
             let data = try Data(contentsOf: url!)
             categoryItem.setValue(data, forKey: "sound")
         } catch  {
             print("NOT INTERNET")
         }
        categoryItem.setValue(translate, forKeyPath: "translate")
        do {
           try managedContext.save()
           self.insertCoreData.append(categoryItem)
           print("insert data Translates success")
        } catch let error as NSError {
           print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    ///insert data to grammarCr Form CoreData
    func insertGrammarFormCoreData(id:Int16, name:String, classify_name:String, classify_id:Int16, status:Int16) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
          return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "GrammarCR", in: managedContext)!
        let grammarItem = NSManagedObject(entity: entity, insertInto: managedContext)
        grammarItem.setValue(id, forKeyPath: "id")
        grammarItem.setValue(name, forKey: "name")
        grammarItem.setValue(classify_id, forKey: "classify_id")
        grammarItem.setValue(classify_name, forKey: "classify_name")
        grammarItem.setValue(status, forKeyPath: "status")
        do {
           try managedContext.save()
           self.insertCoreData.append(grammarItem)
           print("insert data Grammar success")
        } catch let error as NSError {
           print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    ///insert data to grammarTranslateCR Form CoreData
    func insertGrammarTranslateFormCoreData(id:Int16, grammar_id:Int16, language_code:String, translate:String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
          return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Grammar_translateCR", in: managedContext)!
        let gammarTranslateItem = NSManagedObject(entity: entity, insertInto: managedContext)
        gammarTranslateItem.setValue(id, forKeyPath: "id")
        gammarTranslateItem.setValue(translate, forKey: "translate")
        gammarTranslateItem.setValue(grammar_id, forKey: "grammar_id")
        gammarTranslateItem.setValue(language_code, forKey: "language_code")
        do {
           try managedContext.save()
           self.insertCoreData.append(gammarTranslateItem)
           print("insert data Grammar translate success")
        } catch let error as NSError {
           print("Could not save. \(error), \(error.userInfo)")
        }
    }
}

@available(iOS 13.0, *)
extension HomeViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryCoreData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let CategoryCell = CollectionCategory.dequeueReusableCell(withReuseIdentifier: Identifier, for: indexPath) as! HomeCollectionViewCell
        CategoryCell.imageCtegory.image = UIImage(data: categoryCoreData[indexPath.row].value(forKey: "image") as! Data)
        CategoryCell.setNameCategory(name: categoryCoreData[indexPath.row].value(forKey: "name") as! String)
        let str : String = self.getValueLocal(key: "font-size")
        let secStr : NSString = str as NSString
        let fontSize : CGFloat = CGFloat(secStr.doubleValue)
        CategoryCell.nameCategory.font = UIFont(name: "Cabin-Regular", size: fontSize)
        if indexPath.row % 2 == 0 {
            CategoryCell.borderRight.isHidden = false
            CategoryCell.borderLeft.isHidden = true
        }else{
            CategoryCell.borderRight.isHidden = true
            CategoryCell.borderLeft.isHidden = true
        }
        return CategoryCell
    }
}

@available(iOS 13.0, *)
extension HomeViewController:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let category_id = categoryCoreData[indexPath.row].value(forKey: "id") as! Int16
        let category_name = categoryCoreData[indexPath.row].value(forKey: "name") as! String
        self.setValueLocal(value: String(category_id), key: "category_id")
        self.setValueLocal(value: category_name, key: "category_name")
        self.NavigationView(Identifier: "PhraseViewController", animated: true)
    }
}

@available(iOS 13.0, *)
extension HomeViewController: PinterestLayoutDelegate {
  func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
    if indexPath.row == 1 {
        return 196
    }
    return 130
  } 
}

